<?php
    Route::group(['prefix' => 'manage','namespace'=>'App\\Http\\'. $controller .'\\','as' =>'admin.','middleware'=>['web','auth']], function () {
        Route::get('/', function(){
            return redirect()->route('admin.users.index');
        });
        Route::resource('/users','UserController');
        Route::resource('/roles','RoleController');
        Route::resource('/permissions','PermissionController');
    });

?>
