<?php
    Route::group(['prefix' => '','namespace'=>'App\\Http\\'. $controller .'\\','as' =>'front.','middleware'=>['web']], function () {
        Route::view('/','layouts.front_layout');
    });

?>
