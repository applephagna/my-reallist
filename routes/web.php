<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
$controller = 'Controllers\\Admin';
$livewire = 'Livewire\\Admin';

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

include_once('front.php');
include_once('admin.php');
