@extends('layouts.ace_layout')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="col-12">
                          <div class="card dcard">
                            <div class="card-body px-1 px-md-3">
                                <div class="d-flex justify-content-between flex-column flex-sm-row mb-3 px-2 px-sm-0">
                                  <h3 class="text-125 pl-1 mb-3 mb-sm-0 text-secondary-d4">
                                    {{ trans('cruds.user.title') }} {{ trans('global.list') }}
                                  </h3>
                                  <div class="pos-rel ml-sm-auto mr-sm-2 order-last order-sm-0">
                                    <i class="fa fa-search position-lc ml-25 text-primary-m1"></i>
                                    <input type="text" class="form-control w-100 pl-45 radius-1 brc-primary-m4" placeholder="Search ...">
                                  </div>

                                  <div class="mb-2 mb-sm-0">
                                    <a href="{{ route('admin.users.create') }}" class="btn btn-blue px-3 d-block w-100 text-95 radius-round border-2 brc-black-tp10">
                                      <i class="fa fa-plus mr-1"></i>
                                       {{ trans('global.create') }}<span class="d-sm-none d-md-inline"> {{ trans('cruds.user.title_singular') }}</span>
                                    </a>
                                  </div>
                                </div>

                                <table id="simple-table" class="mb-0 table table-borderless table-bordered-x brc-secondary-l3 text-dark-m2 radius-1 overflow-hidden">
                                    <thead class="text-dark-tp3 bgc-grey-l4 text-90 border-b-1 brc-transparent">
                                        <tr>
                                            <th class="text-center pr-0">
                                                <label class="py-0">
                                                <input type="checkbox" class="align-bottom mb-n1 border-2 text-dark-m3">
                                                </label>
                                            </th>
                                            <th>{{ trans('cruds.user.fields.id') }}</th>
                                            <th>{{ trans('cruds.user.fields.profile_photo_path') }}</th>
                                            <th>{{ trans('cruds.user.fields.username') }}</th>
                                            <th>{{ trans('cruds.user.fields.full_name') }}</th>
                                            <th>{{ trans('cruds.user.fields.role') }}</th>
                                            <th>{{ trans('cruds.user.fields.usertype') }}</th>
                                            <th>{{ trans('cruds.user.fields.contact') }} </th>
                                            <th>{{ trans('global.action') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody class="mt-1">
                                        @foreach ($users as $key => $row)
                                            <tr class="bgc-h-yellow-l4 d-style">
                                                <td class="text-center pr-0 pos-rel">
                                                    <div class="position-tl h-100 ml-n1px border-l-4 brc-orange-m1 v-hover">
                                                        <!-- border shown on hover -->
                                                    </div>
                                                    <div class="position-tl h-100 ml-n1px border-l-4 brc-success-m1 v-active">
                                                        <!-- border shown when row is selected -->
                                                    </div>
                                                    <label>
                                                        <input type="checkbox" class="align-middle">
                                                    </label>
                                                </td>
                                                <td>
                                                <a href="#" class="text-blue-d1 text-600 text-95">{{ ++$key }}</a>
                                                </td>
                                                <td class="text-600 text-orange-d2">{{ $row->profile_photo_path }}</td>
                                                <td class="d-none d-sm-table-cell text-dark-d1">{{ $row->username }}</td>
                                                <td class="d-none d-sm-table-cell text-dark text-95">{{ $row->first_name }} {{ $row->last_name }}</td>
                                                <td class="d-none d-sm-table-cell">
                                                    @foreach ($row->roles?? [] as $role)
                                                        <span class="badge badge-sm bgc-success-d1 text-white pb-1 px-25">
                                                            {{ $role->name  }}
                                                        </span>
                                                    @endforeach
                                                </td>
                                                <td class="d-none d-sm-table-cell">
                                                    <span class="badge badge-sm bgc-info-d1 text-white pb-1 px-25">{{ $row->usertype }}</span>
                                                </td>
                                                <td class="text-center pr-0">{{ $row->phone }}</td>
                                                <td>
                                                    <!-- action buttons -->
                                                    <div class="d-none d-lg-flex">
                                                        <a href="{{ route('admin.users.edit',$row->id) }}" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-secondary btn-h-lighter-success btn-a-lighter-success">
                                                            <i class="fa fa-pencil-alt"></i>
                                                        </a>
                                                        <a href="javascript:deleteObject({{ $row->id }})" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-secondary btn-h-lighter-danger btn-a-lighter-danger">
                                                            <i class="fa fa-trash-alt"></i>
                                                        </a>
                                                        <form id="frmDeletePost-{{ $row->id }}" style="display: none" action="{{ route('admin.users.destroy', $row->id) }}" role="form" method="POST" enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr class="border-0 detail-row bgc-white">
                                            <td colspan="8" class="p-0 border-none brc-secondary-l2">
                                            <div class="table-detail collapse" id="table-detail-0">
                                                <div class="row">
                                                <div class="col-12 col-md-10 offset-md-1 p-4">
                                                    <div class="alert bgc-secondary-l4 text-dark-m2 border-none border-l-4 brc-primary-m1 radius-0 mb-0">
                                                    <h4 class="text-primary">
                                                        Row Details
                                                    </h4>
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <!-- table footer -->
                                <div class="d-flex pl-4 pr-3 pt-35 border-t-1 brc-secondary-l3 flex-column flex-sm-row mt-n1px">
                                  <div class="text-nowrap align-self-center align-self-sm-start">
                                    <span class="d-inline-block text-grey-d2">
                                      Showing 1 - 10 of 152
                                  </span>

                                    <select class="ml-3 ace-select no-border angle-down brc-h-blue-m3 w-auto pr-45 text-secondary-d3">
                                      <option value="10">Show 10</option>
                                      <option value="20">Show 20</option>
                                      <option value="50">Show 50</option>
                                    </select>
                                  </div>

                                  <div class="btn-group ml-sm-auto mt-3 mt-sm-0">
                                    <a href="#" class="btn btn-lighter-default btn-bgc-white btn-a-secondary radius-l-1 px-3 border-2">
                                      <i class="fa fa-caret-left mr-1"></i>
                                      Prev
                                    </a>
                                    <a href="#" class="btn btn-lighter-default btn-bgc-white btn-a-secondary radius-r-1 px-3 border-2 ml-n2px">
                                      Next
                                      <i class="fa fa-caret-right ml-1"></i>
                                    </a>
                                  </div>
                                </div>
                            </div><!-- /.card-body -->
                          </div><!-- /.card -->
                        </div><!-- /.col -->
                      </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        function deleteObject(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    document.getElementById('frmDeletePost-'+id).submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        }
    </script>
    <script type="text/javascript">
        jQuery(function($) {
            // highlight simple table row when selected
            function _highlight(row, checked) {
                if (checked) {
                    row.classList.add('active')
                    row.classList.add('bgc-success-l3')
                    row.classList.remove('bgc-h-default-l3')
                }
                else {
                    row.classList.remove('active')
                    row.classList.remove('bgc-success-l3')
                    row.classList.add('bgc-h-default-l3')
                }
            }

            $('#simple-table tbody tr').on('click', function(e)
            {
                var ret = false
                try {
                    // return if clicked on a .btn or .dropdown
                    ret = e.target.classList.contains('btn') || e.target.parentNode.classList.contains('btn')|| e.target.closest('.dropdown') != null
                } catch(err) {}
                if (ret) return
                var inp = this.querySelector('input')
                if(inp == null) return
                if(e.target.tagName != "INPUT") {
                    inp.checked = !inp.checked
                }
                _highlight(this, inp.checked)
            })

            $('#simple-table thead input').on('change', function()
            {
                var checked = this.checked
                $('#simple-table tbody input[type=checkbox]')
                .each(function() {
                    this.checked = checked
                    var row = $(this).closest('tr').get(0)
                    _highlight(row, checked)
                })
            })
        })
    </script>
@endpush
