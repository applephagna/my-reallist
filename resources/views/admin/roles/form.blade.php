<div class="form-group">
    <div class="row">
        {!! Form::label('name', 'Role Name', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-5">
            {!! Form::text('name', null, ["placeholder" => "", "class" => "form-control border-form",  "required"]) !!}
        </div>

        {!! Form::label('slug', 'Slug', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-5">
            {!! Form::text('slug', null, ["placeholder" => "", "class" => "form-control border-form", "required"]) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        {!! Form::label('description', 'Description', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-12">
            {!! Form::textarea('description', null, ["placeholder" => "", "class" => "form-control border-form","rows"=>"2"]) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('permission', 'Permissions', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-12">
            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th width="20%">Group</th>
                    <th class="center">
                        <label class="pos-rel">
                            <input type="checkbox" class="ace" />
                            <span class="lbl"></span>
                        </label>
                    </th>
                    <th>Access</th>
                </tr>
                </thead>
                <tbody>
                @if($permissions && $permissions->count())
                    @foreach($permissions as $key => $permission)
                        <tr>
                            <td><strong>{{$permission->first()->group}}</strong></td>
                            <td class="center first-child">
                                <label>
                                    <input type="checkbox" name="chkIds[]" value="{{ $key }}" class="ace group" />
                                    <span class="lbl"></span>
                                </label>
                            </td>
                            <td>
                                @foreach($permission as $access)
                                    <label>
                                        @if (!isset($role_permission))
                                            {!! Form::checkbox('permission[]', $access->id, false, ['class' => 'ace']) !!}
                                        @else
                                            {!! Form::checkbox('permission[]', $access->id, array_key_exists($access->id, $role_permission), ['class' => 'ace']) !!}
                                        @endif
                                        <span class="lbl"> {{ $access->slug}} </span>
                                    </label>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td colspan="7">No data found.</td></tr>
                @endif
                </tbody>
            </table>
        <div class="control-group">
        </div>
    </div>
</div>
