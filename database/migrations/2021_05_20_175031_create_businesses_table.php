<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->id();
            // $table->increments('id');
            $table->string('name');
            // $table->integer('currency_id')->unsigned();
            // $table->integer('package_id')->unsigned();

            $table->date('start_date')->nullable();
            $table->date('package_expired')->nullable();
            $table->date('package_renew_one')->nullable();
            $table->tinyInteger('package_is_renew')->default(0);


            $table->string('tax_number_1', 100);
            $table->string('tax_label_1', 10);
            $table->string('tax_number_2', 100)->nullable();
            $table->string('tax_label_2', 10)->nullable();
            $table->float('default_profit_percent', 5, 2)->default(0);
            // $table->integer('owner_id')->unsigned();
            // $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('time_zone')->default('Asia/Kolkata');
            $table->tinyInteger('fy_start_month')->default(1);
            $table->enum('accounting_method', ['fifo', 'lifo', 'avco'])->default('fifo');
            $table->decimal('default_sales_discount', 5, 2)->nullable();
            $table->enum('sell_price_tax', ['includes', 'excludes'])->default('includes');

            // $table->foreign('currency_id')->references('id')->on('currencies');
            // $table->foreign('package_id')->references('id')->on('packages');

            $table->unsignedBigInteger('currency_id')->references('id')->on('currencies')->onDelete('cascade');
            $table->unsignedBigInteger('package_id')->references('id')->on('packages')->onDelete('cascade');
            $table->unsignedBigInteger('owner_id')->references('id')->on('users')->onDelete('cascade');


            $table->string('logo')->nullable();
            $table->string('sku_prefix')->nullable();
            $table->boolean('enable_tooltip')->default(1);



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
