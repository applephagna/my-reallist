<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        	'first_name' => 'Super Admin',
            'last_name' => 'Super Admin',
            'username' => 'superadmin',
        	'email' => 'superadmin@gmail.com',
            'phone' => '078343143',
            'usertype' => 'adm',
        	'password' => bcrypt('Phagna@sa225')
        ]);
        $super_admin = Role::create(['name'=>'Super-Admin','slug'=>'super-admin']);
        $user->assignRole([$super_admin->id]);

        $user = User::create([
        	'first_name' => 'Admin',
            'last_name' => 'Administrator',
            'username' => 'admin',
        	'email' => 'admin@gmail.com',
            'phone' => '070393143',
            'usertype' => 'adm',
        	'password' => bcrypt('Phagna@sa225')
        ]);

        $role = Role::create(['name' => 'Admin','slug'=>'admin']);
        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);
        $user->assignRole([$role->id]);
    }
}
