<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::orderBy('id','asc')->paginate(5);
        return view('admin.users.index',compact('users'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $data['roles'] = Role::pluck('name','id')->all();
        return view('admin.users.create',$data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'usertype'  => 'required',
            'roles.*' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        return redirect()->route('admin.users.index')
                        ->with('toast_success','User created successfully');
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.show',compact('user'));
    }

    public function edit($id)
    {
        $data['user'] = User::find($id);
        $data['roles'] = Role::pluck('name','id')->all();
        $data['userRoles'] = $data['user']->roles->pluck('name','id')->all();
        return view('admin.users.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'username' => "required|unique:users,username,$id",
            'email' => "required|email|unique:users,email,$id",
            'phone' => "required|unique:users,phone,$id",
            'password' => "same:confirm-password",
            'usertype'  => 'required',
            'roles.*' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
        $input = $request->all();
        if(!empty($input['password'])){
            $input['password'] = bcrypt($input['password']);
        }else{
            $input = Arr::except($input,array('password'));
        }
        $user = User::find($id);
        $user->update($input);
        $user->roles()->sync($request->input('roles'));
        // DB::table('model_has_roles')->where('model_id',$id)->delete();
        // $user->assignRole($request->input('roles'));

        return redirect()->route('admin.users.index')
                        ->with('toast_success','User updated successfully');
    }

    public function destroy($id)
    {
        User::FindOrFail($id)->delete();
        return redirect()->back();
    }
}
